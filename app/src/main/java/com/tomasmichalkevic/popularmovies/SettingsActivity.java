/*
 * PROJECT LICENSE
 *
 * This project was submitted by Tomas Michalkevic as part of the Nanodegree At Udacity.
 *
 * As part of Udacity Honor code, your submissions must be your own work, hence
 * submitting this project as yours will cause you to break the Udacity Honor Code
 * and the suspension of your account.
 *
 * Me, the author of the project, allow you to check the code as a reference, but if
 * you submit it, it's your own responsibility if you get expelled.
 *
 * Copyright (c) 2018 Tomas Michalkevic
 *
 * Besides the above notice, the following license applies and this license notice
 * must be included in all works derived from this project.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.tomasmichalkevic.popularmovies;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.PreferenceActivity;

public class SettingsActivity extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {

    private static final String KEY_ORDER_PREFERENCE = "orderPrefKey";
    private static final String KEY_FAVOURITES_VIEW_KEY = "favouriteCheckBox";

    private ListPreference orderPreference;
    private CheckBoxPreference favouriteViewPreference;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.activity_preferences);

        orderPreference = (ListPreference) getPreferenceScreen().findPreference(KEY_ORDER_PREFERENCE);
        favouriteViewPreference = (CheckBoxPreference) getPreferenceScreen().findPreference(KEY_FAVOURITES_VIEW_KEY);

        boolean isEnabled = favouriteViewPreference.isChecked();
        if(isEnabled){
            orderPreference.setEnabled(false);
        }else{
            orderPreference.setEnabled(true);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        orderPreference.setSummary("Current: " + orderPreference.getEntry().toString());
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if(key.equals(KEY_FAVOURITES_VIEW_KEY)){
            boolean isEnabled = sharedPreferences.getBoolean(key, false);
            orderPreference.setEnabled(!isEnabled);
        }

        if (key.equals(KEY_ORDER_PREFERENCE)) {
            orderPreference.setSummary("Current: " + orderPreference.getEntry().toString());
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("checkboxChecked", favouriteViewPreference.isChecked());
        outState.putBoolean("checkboxEnabled", favouriteViewPreference.isEnabled());
        outState.putString("orderValue", orderPreference.getValue());
    }

    @Override
    protected void onRestoreInstanceState(Bundle state) {
        super.onRestoreInstanceState(state);
        if(state != null){
            favouriteViewPreference.setChecked(state.getBoolean("checkboxChecked"));
            favouriteViewPreference.setEnabled(state.getBoolean("checkboxEnabled"));
            orderPreference.setValue(state.getString("orderValue"));
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(RESULT_OK, null);
        this.finish();
    }
}
